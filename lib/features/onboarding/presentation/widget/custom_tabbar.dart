import 'package:test_demo/core/constants/colors.dart';
import 'package:flutter/material.dart';

class CustomTabBarItem extends StatelessWidget {
  const CustomTabBarItem({
    super.key,
    this.onTap,
    required this.index,
    required this.isSelected,
    required this.title,
  });

  final void Function()? onTap;
  final int index;
  final bool isSelected;
  final String title;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 40,
        width: 160,
        decoration: BoxDecoration(
            color: isSelected ? k81E6D9 : Colors.white,
            border: isSelected ? null : Border.all(color: kCBD5E0, width: 1),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(index == 0 ? 12 : 0),
              topRight: Radius.circular(index == 2 ? 12 : 0),
              bottomLeft: Radius.circular(index == 0 ? 12 : 0),
              bottomRight: Radius.circular(index == 2 ? 12 : 0),
            )),
        alignment: Alignment.center,
        child: Text(
          title,
          style: TextStyle(
              fontSize: 14,
              letterSpacing: 0.84,
              color: isSelected ? kE6FFFA : k319795,
              fontWeight: FontWeight.w600),
        ),
      ),
    );
  }
}
