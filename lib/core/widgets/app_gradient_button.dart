import 'package:test_demo/core/constants/colors.dart';
import 'package:test_demo/core/constants/gradients.dart';
import 'package:flutter/material.dart';

class AppGradientButton extends StatelessWidget {
  const AppGradientButton({super.key, required this.title, this.onTap});
  final String title;
  final void Function()? onTap;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 40,
        decoration: BoxDecoration(
            gradient: appButtonGradients,
            borderRadius: BorderRadius.circular(12)),
        alignment: Alignment.center,
        child: Text(
          title,
          style: const TextStyle(
              fontSize: 14, color: kE6FFFA, fontWeight: FontWeight.w600, letterSpacing: 0.84),
        ),
      ),
    );
  }
}
