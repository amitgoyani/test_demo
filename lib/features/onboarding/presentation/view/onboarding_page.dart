import 'package:test_demo/core/constants/app_string_constant.dart';
import 'package:test_demo/core/constants/colors.dart';
import 'package:test_demo/core/constants/gradients.dart';
import 'package:test_demo/core/constants/responsive.dart';
import 'package:test_demo/core/widgets/app_gradient_button.dart';
import 'package:test_demo/core/widgets/app_header.dart';
import 'package:test_demo/features/onboarding/presentation/view/tab_view_content.dart';
import 'package:test_demo/features/onboarding/presentation/widget/clippers.dart';
import 'package:test_demo/features/onboarding/presentation/widget/custom_tabbar.dart';
import 'package:flutter/material.dart';

class OnboardingScreen extends StatefulWidget {
  const OnboardingScreen({super.key});

  @override
  State<OnboardingScreen> createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  int currentIndex = 0;
  bool showButtonOnTopBar = false;
  late ScrollController tabScrollController, pageScrollController;

  @override
  void initState() {
    super.initState();
    tabScrollController = ScrollController();
    pageScrollController = ScrollController();
    pageScrollController.addListener(() {
      if (pageScrollController.offset > 325) {
        if (!showButtonOnTopBar) {
          setState(() {
            showButtonOnTopBar = true;
          });
        }
      } else {
        if (showButtonOnTopBar) {
          setState(() {
            showButtonOnTopBar = false;
          });
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: [
          SingleChildScrollView(
            controller: pageScrollController,
            child: Column(
              children: [
                Responsive.isSmallScreen(context)
                    ? SizedBox(
                        height: 700,
                        child: Stack(
                          fit: StackFit.expand,
                          children: [
                            ClipPath(
                              clipper: FirstSectionClipper(),
                              child: Container(
                                decoration: const BoxDecoration(
                                    gradient: appOnboardingJobGradients),
                                child: Column(
                                  children: [
                                    const SizedBox(
                                      height: 85,
                                    ),
                                    const Text(
                                      AppStringConstants.deineJobWebsite,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 42,
                                        letterSpacing: 1.26,
                                        color: k2D3748,
                                        height: 1.2,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                    Expanded(
                                      child: Stack(
                                        children: [
                                          Positioned(
                                            left: -40,
                                            right: -110,
                                            top: 0,
                                            bottom: 0,
                                            child: Image.asset(
                                              AppStringConstants.agreementImage,
                                              fit: BoxFit.contain,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 100,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    : ClipPath(
                  clipper: FirstSectionClipper(),
                      child: Container(
                          height: 550,
                          padding: const EdgeInsets.only(top: 85),
                          decoration: const BoxDecoration(
                              gradient: appOnboardingJobGradients),
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 25.0),
                              child: ConstrainedBox(
                                constraints: const BoxConstraints(
                                  maxWidth: 800,
                                ),
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 3,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          const Text(
                                            AppStringConstants.deineJobWebsite,
                                            style: TextStyle(
                                              fontSize: 42,
                                              letterSpacing: 1.26,
                                              color: k2D3748,
                                              height: 1.2,
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 40,
                                          ),
                                          AppGradientButton(
                                            title: AppStringConstants.registration,
                                            onTap: () {},
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Expanded(
                                      flex: 7,
                                      child: Center(
                                        child: Container(
                                          height: 350,
                                          width: 390,
                                          decoration: const BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Colors.white,
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                    AppStringConstants
                                                        .agreementImage,
                                                  ),
                                                  fit: BoxFit.contain)),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                    ),
                SizedBox(
                  height: Responsive.isSmallScreen(context) ? 27 : 35,
                ),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    height: 40,
                    width: 520,
                    child: ListView.builder(
                      controller: tabScrollController,
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      scrollDirection: Axis.horizontal,
                      itemCount: tabBarItem.length,
                      itemBuilder: (context, index) => CustomTabBarItem(
                        index: index,
                        isSelected: index == currentIndex,
                        title: tabBarItem[index],
                        onTap: () {
                          var width = MediaQuery.of(context).size.width;
                          var offset = 520 - width;
                          if (width < 520) {
                            tabScrollController.animateTo(
                              index == 0 ? 0 : (index == 1 ? (offset/2) : offset),
                              duration: const Duration(milliseconds: 500),
                              curve: Curves.ease,
                            );
                          }
                          setState(() {
                            currentIndex = index;
                          });
                        },
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: Responsive.isSmallScreen(context) ? 30 : 60,
                ),
                TabViewContent(index: currentIndex),
                SizedBox(
                  height: Responsive.isSmallScreen(context) ? 130 : 0,
                ),
              ],
            ),
          ),
          AppHeader(
              showButton: Responsive.isLargeScreen(context)
                  ? showButtonOnTopBar
                  : false),
          if (Responsive.isSmallScreen(context))
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 90,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(12),
                        topRight: Radius.circular(12.0)),
                    boxShadow: [
                      BoxShadow(
                          color: k00000033.withOpacity(0.25),
                          blurRadius: 3,
                          offset: const Offset(0, -1))
                    ]),
                alignment: Alignment.topCenter,
                padding:
                    const EdgeInsets.only(top: 24.0, left: 20.0, right: 20.0),
                child: AppGradientButton(
                  title: AppStringConstants.registration,
                  onTap: () {},
                ),
              ),
            )
        ],
      ),
    );
  }
}

final tabBarItem = [
  AppStringConstants.arbeitnehmer,
  AppStringConstants.arbeitgeber,
  AppStringConstants.temporarburo
];
