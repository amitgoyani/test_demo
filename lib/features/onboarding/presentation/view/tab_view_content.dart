import 'package:dashed_line/dashed_line.dart';
import 'package:test_demo/core/constants/app_string_constant.dart';
import 'package:test_demo/core/constants/colors.dart';
import 'package:test_demo/core/constants/gradients.dart';
import 'package:test_demo/core/constants/responsive.dart';
import 'package:test_demo/features/onboarding/presentation/widget/clippers.dart';
import 'package:flutter/material.dart';

class TabViewContent extends StatelessWidget {
  final int index;

  const TabViewContent({required this.index, super.key});

  @override
  Widget build(BuildContext context) {
    return Responsive.isSmallScreen(context)
        ? _MobileView(index: index)
        : _DesktopView(index: index);
  }
}

class _MobileView extends StatelessWidget {
  final int index;

  const _MobileView({required this.index});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          index == 0
              ? AppStringConstants.dreiEinfacheSchritteZuDeinemNeuenJob
              : index == 1
                  ? AppStringConstants
                      .dreiEinfacheSchritteZuDeinemNeuenMitarbeiter
                  : AppStringConstants
                      .dreiEinfacheSchritteZurVermittlungNeuerMitarbeiter,
          textAlign: TextAlign.center,
          style: const TextStyle(
            height: 1.2,
            fontSize: 21,
            color: k4A5568,
            fontWeight: FontWeight.w500,
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        SizedBox(
          height: 225,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: const EdgeInsets.only(right: 40),
                  child: Image.asset(
                    AppStringConstants.profileDataImage,
                    height: 144.55,
                    width: 219.56,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      const Text(
                        AppStringConstants.one,
                        style: TextStyle(
                          fontSize: 130,
                          height: 1,
                          fontWeight: FontWeight.normal,
                          color: k718096,
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10.0),
                          child: Text(
                            index == 0
                                ? AppStringConstants.erstellenDeinLebenslauf
                                : AppStringConstants
                                    .erstellenDeinUnternehmensprofil,
                            style: const TextStyle(
                              fontSize: 16,
                              letterSpacing: 0.47,
                              fontWeight: FontWeight.normal,
                              color: k718096,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 400,
          child: Stack(
            fit: StackFit.expand,
            children: [
              ClipPath(
                clipper: TabSection2Clipper(),
                child: Container(
                  decoration: const BoxDecoration(
                    gradient: appOnboardingTabGradients,
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const SizedBox(
                    height: 40,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 47.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        const Text(
                          AppStringConstants.two,
                          style: TextStyle(
                            fontSize: 130,
                            height: 1,
                            fontWeight: FontWeight.normal,
                            color: k718096,
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 10.0),
                            child: Text(
                              index == 0
                                  ? AppStringConstants.erstellenDeinLebenslauf
                                  : index == 1
                                      ? AppStringConstants
                                          .erstellenEinJobinserat
                                      : AppStringConstants
                                          .erhalteVermittlungsangebotvonArbeitgeber,
                              style: const TextStyle(
                                fontSize: 16,
                                letterSpacing: 0.47,
                                fontWeight: FontWeight.normal,
                                color: k718096,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 80.0),
                      child: Image.asset(
                        index == 0
                            ? AppStringConstants.taskImage
                            : index == 1
                                ? AppStringConstants.aboutMeImage
                                : AppStringConstants.jobOfferImage,
                        height: 126.55,
                        width: 180.74,
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
        SizedBox(
          height: 360,
          child: Stack(
            clipBehavior: Clip.none,
            children: [
              Positioned(
                top: -30,
                left: -65,
                child: ClipOval(
                  clipper: CustomOvalClipper(),
                  child: Container(
                    height: 360,
                    width: 360,
                    color: kF7FAFC,
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 56.0),
                    child: Row(
                      children: [
                        const Text(
                          AppStringConstants.three,
                          style: TextStyle(
                            fontSize: 130,
                            height: 1,
                            fontWeight: FontWeight.normal,
                            color: k718096,
                          ),
                        ),
                        Expanded(
                          child: Text(
                            index == 0
                                ? AppStringConstants.mitNurEinemKlickBewerben
                                : index == 1
                                    ? AppStringConstants
                                        .wahleDeinenNeuenMitarbeiterAus
                                    : AppStringConstants
                                        .vermittlungNachProvisionOderStundenlohn,
                            style: const TextStyle(
                              fontSize: 16,
                              letterSpacing: 0.47,
                              fontWeight: FontWeight.normal,
                              color: k718096,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: Image.asset(
                      index == 0
                          ? AppStringConstants.personalFileImage
                          : index == 1
                              ? AppStringConstants.swipeProfileImage
                              : AppStringConstants.businessDealImage,
                      height: 210,
                      width: 281,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class _DesktopView extends StatelessWidget {
  final int index;

  const _DesktopView({required this.index});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          top: 420,
          left: 0,
          right: 0,
          child: ClipPath(
            clipper: TabSection2DesktopClipper(),
            child: Container(
              height: 300,
              decoration: const BoxDecoration(
                gradient: appOnboardingTabGradients,
              ),
            ),
          ),
        ),
        Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(
              maxWidth: 800,
            ),
            child: Stack(
              clipBehavior: Clip.none,
              children: [
                Positioned(
                  left: 0,
                  top: 160,
                  child: Container(
                    height: 160,
                    width: 160,
                    decoration: const BoxDecoration(
                      color: Color(0xFFF7FAFC),
                      shape: BoxShape.circle,
                    ),
                  ),
                ),
                Positioned(
                  left: 25,
                  bottom: 70,
                  child: Container(
                    height: 220,
                    width: 230,
                    decoration: const BoxDecoration(
                      color: Color(0xFFF7FAFC),
                      shape: BoxShape.circle,
                    ),
                  ),
                ),
                Positioned(
                  left: 100,
                  top: 300,
                  child: SizedBox(
                    height: 270,
                    width: 375,
                    child: DashedLine(
                      path: arrowPathSecond(270.0, 375.0),
                      color: const Color(0xFF4A5568),
                      dashSpace: 2,
                      dashLength: 2,
                    ),
                  ),
                ),
                Positioned(
                  left: 135,
                  top: 650,
                  child: SizedBox(
                    height: 270,
                    width: 395,
                    child: DashedLine(
                      path: arrowPathFirst(270.0, 395.0),
                      color: const Color(0xFF4A5568),
                      dashSpace: 2,
                      dashLength: 2,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        index == 0
                            ? AppStringConstants
                                .dreiEinfacheSchritteZuDeinemNeuenJob
                            : index == 1
                                ? AppStringConstants
                                    .dreiEinfacheSchritteZuDeinemNeuenMitarbeiter
                                : AppStringConstants
                                    .dreiEinfacheSchritteZurVermittlungNeuerMitarbeiter,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          height: 1.2,
                          fontSize: 36,
                          color: k4A5568,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      SizedBox(
                        height: 225,
                        child: Row(
                          children: [
                            Expanded(
                              flex: 3,
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 12),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    const Text(
                                      AppStringConstants.one,
                                      style: TextStyle(
                                        fontSize: 100,
                                        height: 1,
                                        fontWeight: FontWeight.normal,
                                        color: k718096,
                                      ),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 10.0),
                                        child: Text(
                                          index == 0
                                              ? AppStringConstants
                                                  .erstellenDeinLebenslauf
                                              : AppStringConstants
                                                  .erstellenDeinUnternehmensprofil,
                                          style: const TextStyle(
                                            fontSize: 16,
                                            letterSpacing: 0.47,
                                            fontWeight: FontWeight.w500,
                                            color: k718096,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: Image.asset(
                                AppStringConstants.profileDataImage,
                                fit: BoxFit.contain,
                                height: 170,
                              ),
                            ),
                            const Expanded(flex: 1, child: SizedBox())
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      SizedBox(
                        height: 400,
                        child: Row(
                          children: [
                            Expanded(
                              child: Image.asset(
                                index == 0
                                    ? AppStringConstants.taskImage
                                    : index == 1
                                        ? AppStringConstants.aboutMeImage
                                        : AppStringConstants.jobOfferImage,
                                fit: BoxFit.contain,
                                height: 170,
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 47.0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    const Text(
                                      AppStringConstants.two,
                                      style: TextStyle(
                                        fontSize: 100,
                                        height: 1,
                                        fontWeight: FontWeight.normal,
                                        color: k718096,
                                      ),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 10.0),
                                        child: Text(
                                          index == 0
                                              ? AppStringConstants
                                                  .erstellenDeinLebenslauf
                                              : index == 1
                                                  ? AppStringConstants
                                                      .erstellenEinJobinserat
                                                  : AppStringConstants
                                                      .erhalteVermittlungsangebotvonArbeitgeber,
                                          style: const TextStyle(
                                            fontSize: 16,
                                            letterSpacing: 0.47,
                                            fontWeight: FontWeight.w500,
                                            color: k718096,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 360,
                        child: Row(
                          children: [
                            Expanded(
                              flex: 3,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 80.0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    const Text(
                                      AppStringConstants.three,
                                      style: TextStyle(
                                        fontSize: 100,
                                        height: 1,
                                        fontWeight: FontWeight.normal,
                                        color: k718096,
                                      ),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 10.0),
                                        child: Text(
                                          index == 0
                                              ? AppStringConstants
                                                  .mitNurEinemKlickBewerben
                                              : index == 1
                                                  ? AppStringConstants
                                                      .wahleDeinenNeuenMitarbeiterAus
                                                  : AppStringConstants
                                                      .vermittlungNachProvisionOderStundenlohn,
                                          style: const TextStyle(
                                            fontSize: 16,
                                            letterSpacing: 0.47,
                                            fontWeight: FontWeight.w500,
                                            color: k718096,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 4,
                              child: Image.asset(
                                index == 0
                                    ? AppStringConstants.personalFileImage
                                    : index == 1
                                        ? AppStringConstants.swipeProfileImage
                                        : AppStringConstants.businessDealImage,
                                fit: BoxFit.contain,
                                height: 200,
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Path arrowPathFirst(double height, double width) {
    final path = Path()
      ..moveTo(
        width,
        0,
      )
      ..quadraticBezierTo(
          width * 0.90, height * 0.30, width * 0.62, height * 0.36)
      ..quadraticBezierTo(
          width * 0.25, height * 0.45, width * 0.18, height * 0.70)
      ..moveTo(width * 0.173, height * 0.72)
      ..lineTo(width * 0.16, height * 0.692)
      ..moveTo(width * 0.173, height * 0.72)
      ..lineTo(width * 0.20, height * 0.70);
    return path;
  }

  Path arrowPathSecond(double height, double width) {
    final path = Path()
      ..moveTo(
        0,
        0,
      )
      ..quadraticBezierTo(
          width * 0.10, height * 0.30, width * 0.38, height * 0.36)
      ..quadraticBezierTo(
          width * 0.75, height * 0.45, width * 0.82, height * 0.70)
      ..moveTo(width * 0.827, height * 0.72)
      ..lineTo(width * 0.84, height * 0.69)
      ..moveTo(width * 0.827, height * 0.72)
      ..lineTo(width * 0.80, height * 0.70);
    return path;
  }
}
