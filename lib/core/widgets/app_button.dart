import 'package:test_demo/core/constants/colors.dart';
import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  const AppButton({super.key, required this.title, this.onTap});

  final String title;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 40,
        padding: const EdgeInsets.symmetric(horizontal: 30),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
            border: Border.all(
              color: const Color(0xFFCBD5E0),
            )),
        alignment: Alignment.center,
        child: Text(
          title,
          style: const TextStyle(
            fontSize: 14,
            color: k319795,
            fontWeight: FontWeight.w600,
            letterSpacing: 0.84,
          ),
        ),
      ),
    );
  }
}
