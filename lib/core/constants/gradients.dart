import 'package:flutter/material.dart';

import 'colors.dart';

const appHeaderGradients = LinearGradient(colors: [k319795, k3182CE]);
const appButtonGradients = LinearGradient(colors: [k319795, k3182CE]);
const appOnboardingJobGradients = LinearGradient(
    colors: [kEBF4FF, kE6FFFA],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight);
const appOnboardingTabGradients = LinearGradient(
    colors: [kE6FFFA, kEBF4FF],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight);
