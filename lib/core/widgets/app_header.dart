import 'package:test_demo/core/constants/app_string_constant.dart';
import 'package:test_demo/core/constants/colors.dart';
import 'package:test_demo/core/constants/gradients.dart';
import 'package:flutter/material.dart';
import 'package:test_demo/core/widgets/app_button.dart';

class AppHeader extends StatelessWidget {
  final bool showButton;

  const AppHeader({super.key, required this.showButton});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          height: 5,
          decoration: const BoxDecoration(gradient: appHeaderGradients),
        ),
        Container(
          height: 62,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(12.0),
                  bottomRight: Radius.circular(12.0)),
              boxShadow: [
                BoxShadow(
                    color: k000029.withOpacity(0.2),
                    blurRadius: 6,
                    offset: const Offset(0, 3))
              ]),
          alignment: Alignment.centerRight,
          padding: const EdgeInsets.only(right: 17),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (showButton) ...[
                const Text(
                  "Jetzt Klicken",
                  style: TextStyle(
                    letterSpacing: 0.84,
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                    color: Color(0xFF4A5568),
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                const AppButton(title: "Kostenlos Registrieren"),
                const SizedBox(
                  width: 20,
                ),
              ],
              const Text(
                AppStringConstants.login,
                style: TextStyle(
                  letterSpacing: 0.84,
                  fontWeight: FontWeight.w600,
                  fontSize: 14,
                  color: k319795,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
