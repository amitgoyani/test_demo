import 'package:flutter/material.dart';

const k319795 = Color(0xff319795);
const k3182CE = Color(0xff3182CE);
const kEBF4FF = Color(0xffEBF4FF);
const kE6FFFA = Color(0xffE6FFFA);
const k000029 = Color(0xff000029);
const k2D3748 = Color(0xff2D3748);
const k00000033 = Color(0xff000033);
const kCBD5E0 = Color(0xffCBD5E0);
const k81E6D9 = Color(0xff81E6D9);
const k4A5568 = Color(0xff4A5568);
const k718096 = Color(0xff718096);
const kF7FAFC = Color(0xffF7FAFC);

const buttonPrimaryColor = Color(0xffFF8933);
