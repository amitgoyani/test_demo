class AppStringConstants {
  const AppStringConstants._();

  static const mainTitle = 'Deine Job Website';
  static const login = 'Login';
  static const deineJobWebsite = 'Deine Job\nwebsite';
  static const registration = 'Kostenlos Registrieren';
  static const arbeitnehmer = 'Arbeitnehmer';
  static const arbeitgeber = 'Arbeitgeber';
  static const temporarburo = 'Temporärbüro';
  static const dreiEinfacheSchritteZuDeinemNeuenJob =
      'Drei einfache Schritte\nzu deinem neuen Job';
  static const one = '1. ';
  static const two = '2. ';
  static const three = '3. ';
  static const erstellenDeinLebenslauf = 'Erstellen dein Lebenslauf';
  static const erstellenDeinUnternehmensprofil = 'Erstellen dein Unternehmensprofil';
  static const erstellenEinJobinserat = 'Erstellen ein Jobinserat';
  static const erhalteVermittlungsangebotvonArbeitgeber = 'Erhalte Vermittlungs- angebot von Arbeitgeber';
  static const mitNurEinemKlickBewerben = 'Mit nur einem Klick bewerben';
  static const dreiEinfacheSchritteZuDeinemNeuenMitarbeiter =
      'Drei einfache Schritte\nzu deinem neuen Mitarbeiter';
  static const wahleDeinenNeuenMitarbeiterAus =
      'Wähle deinen neuen Mitarbeiter aus';
  static const dreiEinfacheSchritteZurVermittlungNeuerMitarbeiter='Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter'; 
  static const vermittlungNachProvisionOderStundenlohn='Vermittlung nach Provision oder Stundenlohn';   

  static const agreementImage = 'assets/undraw_agreement_aajr@2x.png';
  static const profileDataImage = 'assets/undraw_Profile_data_re_v81r@2x.png';
  static const taskImage = 'assets/undraw_task_31wc@2x.png';
  static const personalFileImage = 'assets/undraw_personal_file_222m@2x.png';
  static const aboutMeImage = 'assets/undraw_about_me_wa29@2x.png';
  static const swipeProfileImage = 'assets/undraw_swipe_profiles1_i6mr@2x.png';
  static const businessDealImage = 'assets/undraw_business_deal_cpi9@2x.png';
  static const jobOfferImage = 'assets/undraw_job_offers_kw5d@2x.png';
}
